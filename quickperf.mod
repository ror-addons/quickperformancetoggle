<?xml version="1.0" encoding="UTF-8"?>
<!-- this file's contents are released under Creative Commons Attribution-Noncommercial-Share Alike 3.0
     http://creativecommons.org/licenses/by-nc-sa/3.0/ -->
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="Quick Performance Toggle" version="1.5" date="17/03/2011">
        <Author name="Tempest" email="icona@gmx.net" />
        <Description text="Allows you to toggle Warhammer's performance setting using either '/perf#' (Chat with installed LibSlash) or '/script perf#()' (Macros and Chat) where # is L, M, H, U, 1, 2 or C." />
		<VersionSettings gameVersion="1.4.1" />
		<Dependencies>
			<Dependency name="LibSlash" optional="true" />
		</Dependencies>
        <Files>
			<File name="quickperf.lua" />
		</Files>
        <OnInitialize>
            <CallFunction name="quickperf.OnInitialize" />
        </OnInitialize>
		<OnUpdate/>
		<OnShutdown/>
		<WARInfo>
			<Categories>
				<Category name="RVR" />
				<Category name="COMBAT" />
				<Category name="SYSTEM" />
				<Category name="OTHER" />
			</Categories>
		</WARInfo>
    </UiMod>
</ModuleFile> 