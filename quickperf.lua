-- Warhammer Online Quick Performance Toggle
-- Version 1.5
-- Icona / Tempest (icona@gmx.net)
-- Feel free to e-mail me regarding new ideas, changes, bugs, comments, etc.

-- this file's contents are released under Creative Commons Attribution-Noncommercial-Share Alike 3.0
-- http://creativecommons.org/licenses/by-nc-sa/3.0/

quickperf = {}

function quickperf.OnInitialize()
  if LibSlash then
    LibSlash.RegisterSlashCmd("perfL", perfL)
	LibSlash.RegisterSlashCmd("perfM", perfM)
	LibSlash.RegisterSlashCmd("perfH", perfH)
	LibSlash.RegisterSlashCmd("perf1", perf1)
	LibSlash.RegisterSlashCmd("perf2", perf2)
	LibSlash.RegisterSlashCmd("perfC", perfC)
	LibSlash.RegisterSlashCmd("perfU", perfU)
  end
end

function perfL()
  SystemData.Settings.Performance.perfLevel = SystemData.Settings.Performance.PERF_LEVEL_LOW
  BroadcastEvent(SystemData.Events.USER_SETTINGS_CHANGED)
end

function perfM()
  SystemData.Settings.Performance.perfLevel = SystemData.Settings.Performance.PERF_LEVEL_MEDIUM
  BroadcastEvent(SystemData.Events.USER_SETTINGS_CHANGED)
end

function perfH()
  SystemData.Settings.Performance.perfLevel = SystemData.Settings.Performance.PERF_LEVEL_HIGH
  BroadcastEvent(SystemData.Events.USER_SETTINGS_CHANGED)
end

function perfU()
  SystemData.Settings.Performance.perfLevel = SystemData.Settings.Performance.PERF_LEVEL_VERY_HIGH
  BroadcastEvent(SystemData.Events.USER_SETTINGS_CHANGED)
end

function perf1()
  SystemData.Settings.Performance.perfLevel = SystemData.Settings.Performance.PERF_LEVEL_CUSTOM1
  BroadcastEvent(SystemData.Events.USER_SETTINGS_CHANGED)
end

function perf2()
  SystemData.Settings.Performance.perfLevel = SystemData.Settings.Performance.PERF_LEVEL_CUSTOM2
  BroadcastEvent(SystemData.Events.USER_SETTINGS_CHANGED)
end

function perfC()
  if SystemData.Settings.Performance.perfLevel == SystemData.Settings.Performance.PERF_LEVEL_CUSTOM1 then
    SystemData.Settings.Performance.perfLevel = SystemData.Settings.Performance.PERF_LEVEL_CUSTOM2
  else
    SystemData.Settings.Performance.perfLevel = SystemData.Settings.Performance.PERF_LEVEL_CUSTOM1
  end
  BroadcastEvent(SystemData.Events.USER_SETTINGS_CHANGED)
end

-- function call alternatives
perfl = perfL
perfm = perfM
perfh = perfH
perfc = perfC
perfu = perfU